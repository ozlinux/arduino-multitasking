﻿
## Arduino multitasking example

## Description
This project shows how to run multiple tasks on a small device. There are many ways to do that but on a small Arduino the choices are limited.

## Installation
In order to build and run this example, you need these things:

- Arduino Uno
- Arduino sensor, in this case the temperature+humidity sensor DHT22
- The two libraries: Adafruit Sensor-master, and DHT-sensor-library-master

The libraries can be dowloaded here -

https://www.makerguides.com/wp-content/uploads/2019/02/Adafruit_Sensor-master.zip

https://www.makerguides.com/wp-content/uploads/2019/02/DHT-sensor-library-master.zip

Add the libraries to Arduino IDE and they should be usable right away.

## Usage
This program has three tasks:

- Read temperature and humidity from DHT22 sensor
- Write the reading to the serial port
- Flash the on-board LED

In order to do these things with different intervals, the program uses the millis() function of Arduino to determine when each task should run. For simplicity, each task keeps track of its own timing and decides to run or not. For a more advanced system, that logic should be kept central in a task manager.

## Support
In the source folder, run the command *doxygen* to generate HTML reference documentation for the project.

## Roadmap
This is the first example of Arduino code, but I hope to add more examples in the future.

## Contributing
If you want to add your examples to this repository then send me a message to let me know.

## Authors and acknowledgment
This was written by Peter Andreasen for the online interest group named OZLINUX which is a Danish group of radio amateurs who are also interested in programming small devices.

## License
See the file LICENSE for the license, which is MIT.

## Project status
This is the first project so all this documentation is probably overkill.


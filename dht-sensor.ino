/**
 * @file dht-sensor.ino
 * @brief Project file for sensor example project
 * @author Peter Andreasen
 * @date 2022-12-29
 */

#include <Adafruit_Sensor.h>
#include <DHT.h>

/** The pin where sensor is connected */
#define DHTPIN 2

/** Set DHT type, uncomment whatever type you're using */
//#define DHTTYPE DHT11   // DHT 11 
#define DHTTYPE DHT22   // DHT 22  (AM2302)
//#define DHTTYPE DHT21   // DHT 21 (AM2301)

/** Create DHT sensor for normal 16mhz Arduino */
DHT dht = DHT(DHTPIN, DHTTYPE);

/** Read temp+humidity from sensor */
void dhtTask();

/** Flash the LED */
void blinkTask();

/** Write readings to serial port */
void printTask();

/** Read interval for sensor 2 second */
const unsigned long dhtInterval = 2000;

/** Flashing period for LED 0.25 second */
const unsigned long blinkInterval = 250;

/** Interval between sending data to serial port */
const unsigned long printInterval = 2000;

/** Latest temperature reading from sensor */
float temperature;

/** Latest humidity reading from sensor */
float humidity;

/** Calculated heat index (percieved heat) */
float heatindex;

/** Initialise things */
void setup() {
  Serial.begin(9600);
  pinMode(LED_BUILTIN, OUTPUT);
  dht.begin();
}

/** This is called by Arduino, no need to make an endless loop inside */
void loop() {
  dhtTask();
  blinkTask();
  printTask();
  delay(10);
}

void dhtTask()
{
  static unsigned long lastRun = 0;
  if (millis() > (lastRun + dhtInterval)) {
    lastRun = millis();
  } else {
    return;
  }
  // Reading temperature or humidity takes about 250 milliseconds.
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  // Read the humidity in %:
  humidity = dht.readHumidity();
  // Read the temperature as Celsius:
  temperature = dht.readTemperature();
  // Check if any reads failed and exit early (to try again):
  if (isnan(humidity) || isnan(temperature)) {
    Serial.println("Failed to read from DHT sensor!");
    return;
  }
  // Compute heat index in Celsius:
  heatindex = dht.computeHeatIndex(temperature, humidity, false);
}

void printTask()
{
  static unsigned long lastRun = 0;
  if (millis() > (lastRun + printInterval)) {
    lastRun = millis();
  } else {
    return;
  }
  Serial.print("Humidity: ");
  Serial.print(humidity);
  Serial.print("% ");
  Serial.print("Temperature: ");
  Serial.print(temperature);
  Serial.print("\xC2\xB0");
  Serial.print("C ");
  Serial.print("Heat index: ");
  Serial.print(heatindex);
  Serial.print("\xC2\xB0");
  Serial.println("C");
}

void blinkTask()
{
  static unsigned long lastRun = 0;
  static bool ledOn = false;
  if (millis() > (lastRun + blinkInterval)) {
    lastRun = millis();
  } else {
    return;
  }
  ledOn = !ledOn;
  digitalWrite(LED_BUILTIN, ledOn?HIGH:LOW);
}
